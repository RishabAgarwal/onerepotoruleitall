import numpy as np
import argparse
import cv2 as cv
import matplotlib.pyplot as plt
import os
# from yolo_utils import


def show_image(img, name, output_folder):
    plt.imshow(cv.cvtColor(img, cv.COLOR_BGR2RGB))
    my_path = output_folder + '/' + name
    cv.imwrite(my_path, img)


def draw_labels_and_boxes(img, boxes, confidences, classids, idxs, colors, labels, name):
    # If there are any detections
    global counter
    flag = 0
    if len(idxs) > 0:
        for i in idxs.flatten():
            # Get the bounding box coordinates
            x, y = boxes[i][0], boxes[i][1]
            w, h = boxes[i][2], boxes[i][3]

            # Get the unique color for this class
            color = [int(c) for c in colors[classids[i]]]
            if(len(confidences) > 0):
                if flag == 0:
                    flag = 1
                    counter = counter + 1
            # Draw the bounding box rectangle and label on the image
                cv.rectangle(img, (x, y), (x+w, y+h), color, 2)
                text = "{}: {:4f}".format(labels[classids[i]], confidences[i])
                cv.putText(img, text, (x, y-5),
                           cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    return img


def generate_boxes_confidences_classids(outs, height, width, tconf):
    boxes = []
    confidences = []
    classids = []

    for out in outs:
        for detection in out:
            # Get the scores, classid, and the confidence of the prediction
            scores = detection[5:]
            classid = np.argmax(scores)
            confidence = scores[classid]

            # Consider only the predictions that are above a certain confidence level
            if confidence > tconf:
                box = detection[0:4] * np.array([width, height, width, height])
                centerX, centerY, bwidth, bheight = box.astype('int')

                # Using the center x, y coordinates to derive the top
                # and the left corner of the bounding box
                x = int(centerX - (bwidth / 2))
                y = int(centerY - (bheight / 2))

                # Append to list
                boxes.append([x, y, int(bwidth), int(bheight)])
                confidences.append(float(confidence))
                classids.append(classid)

    return boxes, confidences, classids


def infer_image(net, layer_names, height, width, img, colors, labels, confidence, threshold, name):

    blob = cv.dnn.blobFromImage(
        img, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)

    outs = net.forward(layer_names)

    boxes, confidences, classids = generate_boxes_confidences_classids(
        outs, height, width, confidence)

    # Apply Non-Maxima Suppression to suppress overlapping bounding boxes
    idxs = cv.dnn.NMSBoxes(boxes, confidences, confidence, threshold)

    img = draw_labels_and_boxes(
        img, boxes, confidences, classids, idxs, colors, labels, name)

    return img


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(filename)
    return images


PARAMS = []
if __name__ == '__main__':
    global counter
    counter = 0

    parser = argparse.ArgumentParser()

    parser.add_argument('-w', '--weights',
                        type=str,
                        default='./weights/model-A.weights',
                        help='The path to the weights')
    parser.add_argument('-l', '--label',
                        type=str,
                        default='./classes.txt',
                        help='The path to the label')
    parser.add_argument('-c', '--config',
                        type=str,
                        default='./training/yolov3.cfg',
                        help='The path to the config file')
    parser.add_argument('-i', '--images',
                        type=str,
                        default='./test-images',
                        help='The path to the image folder')
    parser.add_argument('-C', '--confidence',
                        type=float,
                        default=0.3,
                        help='min confidence')
    parser.add_argument('-o', '--out',
                        type=str,
                        default='./output',
                        help='output folder')
    PARAMS, unparsed = parser.parse_known_args()

    labels_path = PARAMS.label
    weights_path = PARAMS.weights
    config_path = PARAMS.config
    confidence = PARAMS.confidence
    threshold = 0.3
    folder = PARAMS.images
    output_folder = PARAMS.out

    image_names = load_images_from_folder(folder)

    labels = open(labels_path).read().strip().split('\n')

    colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')

    net = cv.dnn.readNetFromDarknet(config_path, weights_path)

    layer_names = net.getLayerNames()
    layer_names = [layer_names[i[0] - 1]
                   for i in net.getUnconnectedOutLayers()]

    for i in image_names:
        img = cv.imread(folder + '/' + i)
        height, width = img.shape[:2]

        img = infer_image(net, layer_names, height, width, img,
                          colors, labels, confidence, threshold, i)
        show_image(img, i, output_folder)
    print(str(counter) + '/' + str(len(image_names)))
